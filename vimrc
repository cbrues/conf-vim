" Pathogen plugin manager
" mkdir -p ~/.vim/autoload ~/.vim/bundle && \
"    curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim

" Lightline status/tabline
" git clone https://github.com/itchyny/lightline.vim ~/.vim/bundle/lightline.vim
"
" vim-fugative (git)
" cd ~/.vim/bundle
" git clone https://github.com/tpope/vim-fugitive.git
" vim -u NONE -c 'helptags vim-fugitive/doc' -c q
"
" NERDTree
" git clone https://github.com/scrooloose/nerdtree.git ~/.vim/bundle/nerdtree
" git clone https://github.com/Xuyuanp/nerdtree-git-plugin.git ~/.vim/bundle/nerdtree-git-plugin
"
" OpenFileUnderCursor
" git clone https://github.com/amix/open_file_under_cursor.vim.git ~/.vim/bundle/open_file_under_cursor

execute pathogen#infect()

let g:lightline = {
	\ 'colorscheme': 'landscape',
	\ 'active': {
	\	'left': [ [ 'mode', 'paste' ],
	\	[ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
	\ },
	\ 'component_function': {
	\   'gitbranch': 'fugitive#head'
	\ },
	\ }

colorscheme landscape
syntax on
filetype plugin indent on

" General options
set t_Co=256
set laststatus=2
set showtabline=2
set number
set nowrap
set backspace=eol,start,indent
set whichwrap+=<,>,h,l
set lazyredraw
set encoding=utf8
set ffs=unix,dos,mac

" Ignore garbage files
set wildignore=*.o,*~,*.pyc
if has("win16") || has("win32")
    set wildignore+=.git\*,.hg\*,.svn\*
else
    set wildignore+=*/.git/*,*/.hg/*,*/.svn/*,*/.DS_Store
endif

" Search
set magic
set showmatch
set hlsearch
set incsearch
set ignorecase
set smartcase

" Highlight matching brackets
set showmatch
set mat=2

" Backup files
set nobackup
set noswapfile
set nowb

" Tabs
set tabstop=4
set shiftwidth=4
set noexpandtab
set smarttab

" Commands
command W w !sudo tee % > /dev/null
" Smart way to move between windows
map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-h> <C-W>h
map <C-l> <C-W>l

" Pagup/down
nmap K <C-B><bar>zz
nmap J <C-F><bar>zz
vmap K <C-B><bar>zz
vmap J <C-F><bar>zz

" NERDTree
let g:NERDTreeWinPos = "right"
let NERDTreeShowHidden=0
let NERDTreeIgnore = ['\.pyc$', '__pycache__']
let g:NERDTreeWinSize=35
" Simplify expand/collapse arrows
let g:NERDTreeDirArrowExpandable = '+'
let g:NERDTreeDirArrowCollapsible = '-'
" Open NERDTree instead of default directory browser
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | endif
" Auto close if only NERDTree is open
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
let g:NERDTreeIndicatorMapCustom = {
    \ "Modified"  : "M",
    \ "Staged"    : "S",
    \ "Untracked" : "U",
    \ "Renamed"   : "R",
    \ "Unmerged"  : "U",
    \ "Deleted"   : "D",
    \ "Dirty"     : "*",
    \ "Clean"     : "",
    \ 'Ignored'   : 'I',
    \ "Unknown"   : "?"
    \ }

